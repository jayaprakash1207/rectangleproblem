public class Square implements Shape {
    public int side;

    public Square(int side) {
        this.side=side;
    }
    public int area(){
        return (int) Math.pow(side,2);
    }

    public int perimeter() {
        return 4 * side;
    }
}
