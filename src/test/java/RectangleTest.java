import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
public class RectangleTest {

    @Test
    public void shouldReturnThirtyAsAreaWhenTheDimensionsAreTenAndThree(){
        Shape rectangle = new Rectangle(10,3);

        int actualArea = rectangle.area();

        assertEquals(30,actualArea);
    }

    @Test
    public void shouldReturnTwentySixAsPerimeterWhenTheDimensionsAreTenAndThree(){
        Shape rectangle = new Rectangle(10,3);

        int actualPerimeter = rectangle.perimeter();

        assertEquals(26,actualPerimeter);
    }
}
