import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SquareTest {

    @Test
    public void shouldReturnTwentyFiveAsAreaWhenTheSideOfTheSquareIsFive() {
        Shape square = new Square(5);

        int actualArea = square.area();

        assertEquals(25,actualArea);
    }

    @Test
    public void shouldReturnSixtyFourAsPerimeterWhenTheSideOfTheSquareIsSixteen() {
        Shape square = new Square(16);

        int actualPerimeter = square.perimeter();

        assertEquals(64,actualPerimeter);
    }
}